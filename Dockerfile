FROM alpine:latest

USER root

RUN apk upgrade
RUN apk add --no-cache \
    python3 \
    python3-dev

RUN pip3 install --no-cache-dir --upgrade pip

RUN pip3 install requests flask flask-restful Flask-PyMongo
COPY quarantine_rest quarantine_rest
COPY entrypoint.sh entrypoint.sh
RUN chmod ugo+x entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
