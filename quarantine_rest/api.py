from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://root:example@mongo:27017/test?authSource=admin&authMechanism=SCRAM-SHA-1"
api = Api(app)
mongo = PyMongo(app)

class SimpleQuarantine(Resource):
    def get(self):
        test_uuids = [uuid['uuid'] for uuid in mongo.db.tests.find()]
        return {'failed_test_uuids': test_uuids}

    def post(self):
        uuid = request.form['uuid']
        mongo.db.tests.insert({"uuid": uuid})
        return {'uuid': uuid}

    def delete(self):
        uuid = request.form['uuid']
        mongo.db.tests.remove({"uuid": uuid})
        return {'uuid': uuid}

api.add_resource(SimpleQuarantine, '/quarantine/tests')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
